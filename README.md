# Introduction

A WordPress Starter with Roots projects, including Docker, Acorn, Bedrock, Sage, and Trellis. PHP 8 & MySQL 8.

While [bedrock/trellis/sage](https://roots.io) is a very well thought project, and makes the complete development lifecycle i.e. `design -> code -> release -> deploy` a breeze, the trellis dependency on vagrant makes it slow to develop. This is my attempt to create a boiler plate. The project structure is loosely inspired from [trellis recommended structure](https://roots.io/trellis/docs/installing-trellis/).

## Project Structure

### Bedrock

[bedrock](https://roots.io/bedrock) is present at `site` folder

### Sage

[sage](https://roots.io/sage) is present at `/site/web/app/themes/custom-theme`

### Trellis

[Trellis](https://roots.io/trellis) is at `/trellis`

## Getting Started

First clone the project.

```bash
git clone --depth=1 https://gitlab.com/xyz-starters-and-templates/workpress-bedrock-sage-acorn-starter.git $PROJECT_NAME && rm -rf $PROJECT_NAME/.git
cd $PROJECT_NAME
git init
cp site/.env.example site/.env
```

where `$PROJECT_NAME` is your project name e.g. `www.example.com` or `blog.your-name.com` or whatever.

### Trellis

To upgrade to latest trellis

```bash
cd $PROJECT_NAME && rm -rf trellis && git clone --depth=1 git@github.com:roots/trellis.git && rm -rf trellis/.git
```

### Sage

We default using `custom-theme` for the init theme, but if you need another themes name / multi theme, you can follow these:

```bash
cd $PROJECT_NAME
unlink theme
cd site/web/app/themes
composer create-project roots/sage $THEME_NAME
cd ../../../../
ln -s site/web/app/themes/$THEME_NAME theme
```

you can take a look at `docker-compose.yml`. All the docker related files are at `/docker` folder. This is a minamal setup, to get you quickly started wtih docker, bedrock, trellis and sage without loosing any of the benefits of either of these. Follow the instructions on each of the package documentation to get started on all them. Now for the fireworks ...

```bash
docker-compose up --build --force-recreate -d
```

## Bonus

Everytime the `wp` image runs, it will keep your `composer` and `composer.lock` upto date with latest packages.

To install more npm package to themes, lets say `turbolinks` npm package

```bash
docker exec -it $THEME_CONTAINER yarn add turbolinks
```

you can get the name of the theme container by doing `docker ps` when your docker-compose is running.